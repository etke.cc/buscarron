Hello,
We're thrilled to share that your Matrix server order is confirmed! 🎉

No need for additional details at this moment; we'll keep it simple:

**Payment Instructions**:

1. Visit our [membership page](https://etke.cc/membership).
2. Select the "By Complexity" tier.
3. Set the custom price to $104.
4. Subscribe on Ko-Fi with the same email address you used for this order (test@test.com).

Once your payment is confirmed, we'll promptly initiate the setup of your Matrix server. Look forward to a new email that will guide you through the onboarding process with all the necessary details.

Please, ensure [all mandatory ports are open](https://etke.cc/order/status/#ports-and-firewalls).

Please, add the following DNS entries:

* @    A record    1.2.3.4
* matrix    A record    1.2.3.4
* buscarron    CNAME record    matrix.example.com.
* cinny    CNAME record    matrix.example.com.
* element    CNAME record    matrix.example.com.
* etherpad    CNAME record    matrix.example.com.
* firezone    CNAME record    matrix.example.com.
* funkwhale    CNAME record    matrix.example.com.
* social    CNAME record    matrix.example.com.
* hydrogen    CNAME record    matrix.example.com.
* jitsi    CNAME record    matrix.example.com.
* languagetool    CNAME record    matrix.example.com.
* linkding    CNAME record    matrix.example.com.
* miniflux    CNAME record    matrix.example.com.
* ntfy    CNAME record    matrix.example.com.
* peertube    CNAME record    matrix.example.com.
* radicale    CNAME record    matrix.example.com.
* schildichat    CNAME record    matrix.example.com.
* stats    CNAME record    matrix.example.com.
* sygnal    CNAME record    matrix.example.com.
* kuma    CNAME record    matrix.example.com.
* vault    CNAME record    matrix.example.com.
* matrix    TXT record    v=spf1 ip4:1.2.3.4 -all
* _dmarc.matrix    TXT record    v=DMARC1; p=quarantine;
* default._domainkey.matrix    TXT record    v=DKIM1; k=rsa; p=TODO
* matrix    MX record    0 matrix.example.com.

To check the status of your order and stay updated, please keep an eye on your [Order Status Page](https://etke.cc/order/status/#a379a6f6eeafb9a55e378c118034e2751e682fab9f2d30ab13d2125586ce1947).

Got any questions? Feel free to reply to this email - we're here to assist you!

We're genuinely excited to serve you and provide a top-notch Matrix server experience.

Best regards,

etke.cc