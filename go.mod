module gitlab.com/etke.cc/buscarron

go 1.22

toolchain go1.23.0

require (
	github.com/VictoriaMetrics/metrics v1.35.1
	github.com/archdx/zerolog-sentry v1.8.4
	github.com/getsentry/sentry-go v0.28.1
	github.com/hashicorp/golang-lru/v2 v2.0.7
	github.com/labstack/echo/v4 v4.12.0
	github.com/lib/pq v1.10.9
	github.com/mattevans/postmark-go v1.0.0
	github.com/microcosm-cc/bluemonday v1.0.27
	github.com/rs/zerolog v1.33.0
	github.com/stretchr/testify v1.9.0
	github.com/ziflex/lecho/v3 v3.7.0
	gitlab.com/etke.cc/go/echo-basic-auth v1.1.0
	gitlab.com/etke.cc/go/env v1.2.0
	gitlab.com/etke.cc/go/healthchecks/v2 v2.2.0
	gitlab.com/etke.cc/go/pricify v0.0.0-20240709101247-74e2796c2d3d
	gitlab.com/etke.cc/go/psd v1.1.2
	gitlab.com/etke.cc/go/redmine v0.0.0-20240806112048-db474fffab03
	gitlab.com/etke.cc/go/secgen v1.2.0
	gitlab.com/etke.cc/go/validator/v2 v2.2.0
	gitlab.com/etke.cc/linkpearl v0.0.0-20240731121617-2241628d0fda
	golang.org/x/net v0.28.0
	golang.org/x/text v0.17.0
	golang.org/x/time v0.6.0
	maunium.net/go/mautrix v0.20.0
	modernc.org/sqlite v1.32.0
)

require (
	blitiri.com.ar/go/spf v1.5.1 // indirect
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/buger/jsonparser v1.1.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/css v1.0.1 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/labstack/gommon v0.4.2 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mikesmitty/edkey v0.0.0-20170222072505-3356ea4e686a // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/ncruces/go-strftime v0.1.9 // indirect
	github.com/nixys/nxs-go-redmine/v5 v5.1.0 // indirect
	github.com/petermattis/goid v0.0.0-20240813172612-4fcff4a6cae7 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	github.com/rogpeppe/go-internal v1.10.0 // indirect
	github.com/stretchr/objx v0.5.2 // indirect
	github.com/tidwall/gjson v1.17.3 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	github.com/tidwall/sjson v1.2.5 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fastrand v1.1.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	github.com/valyala/histogram v1.2.0 // indirect
	github.com/yuin/goldmark v1.7.4 // indirect
	gitlab.com/etke.cc/go/trysmtp v1.1.3 // indirect
	go.mau.fi/util v0.7.0 // indirect
	golang.org/x/crypto v0.26.0 // indirect
	golang.org/x/exp v0.0.0-20240808152545-0cdaa3abc0fa // indirect
	golang.org/x/sys v0.24.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	modernc.org/gc/v3 v3.0.0-20240801135723-a856999a2e4a // indirect
	modernc.org/libc v1.59.9 // indirect
	modernc.org/mathutil v1.6.0 // indirect
	modernc.org/memory v1.8.0 // indirect
	modernc.org/strutil v1.2.0 // indirect
	modernc.org/token v1.1.0 // indirect
)
